# NeoID 

Tutorial de instalação e remoção do certificado digital em nuvem NeoID da [SERPRO](https://serpro.gov.br) no sistema operacional Debian GNU/Linux

Ambiente 64 bits:

- [Debian](https://debian.org) 10.4.0
- [NeoID](https://servicos.serpro.gov.br/neoid) 1.5.2

## libssl1.0.0

Instalando dependências

```
# apt-get install multiarch-support debconf
```

Baixando

```
$ wget -O /tmp/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb \
    http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb
```

Instalando

```
# dpkg -i /tmp/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb
```

## NeoID

Instalando dependências

```
# apt-get install libnss3-tools libnotify-bin notify-osd
```

Baixando

```
$ wget --no-check-certificate -O /tmp/neoid-1.5.2-amd64.deb \
    https://neoid.estaleiro.serpro.gov.br/downloads/neoid-1.5.2-amd64.deb
```

Instalando

```
# dpkg -i /tmp/neoid-1.5.2-amd64.deb
```

Após a instalação `NeoID` estará acessível no menu de aplicativos nas categorias Settings e Security ou pelo console `neoid`

## Remoção

Removendo `libssl1.0.0`, `NeoID` e suas dependências 

```
# apt-get remove --purge libssl1.0.0 neoid-desktop
# apt autoremove
```
